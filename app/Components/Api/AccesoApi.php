<?php

namespace App\Components\Api;

use Validator;
use App\Components\Api\ClientApi;
use App\Components\Api\Exceptions\InternalApiException;
use Carbon\Carbon;
use Illuminate\Validation\Rule;

class AccesoApi
{
	
	protected $respuestaApi;

	protected function conectApi(string $url, array $params,string $method)
	{
		try{

			$conexion = new ClientApi($url, $params, $method);
			$this->respuestaApi = $conexion->request();
			
			return true;

		}catch(Exceptions $e){

			return false;
		}

	}

	/**
	* Valida que el array recibido en el body de la respuesta tiene la estructura correcta
	**/
	protected function validateSchemaResponseBody(array $ruleSchema)
	{
		
		$validator =  Validator::make($this->respuestaApi, $ruleSchema);

		if ($validator->fails()) {

			throw new InternalApiException("Esquema respuesta no es el esperado");
		}
		return true;
	}
	/*
	* Retorna el token valido
	*/
	protected function getValidToken()
	{
		try{

			$auth = app(\App\Components\Api\AuthApi::class)->getAuthToken();

			$token = $auth->token;

			return ! empty($token) ? $token : null;

		}catch (Exception  $e) {

            throw new UnauthorizedException($msg, 403);
        }
	}

}