<?php

namespace App\Components\Api;

/**
* Metodos obligatorios en todas las apis para acceder a informes
*
**/

interface AccessClientInterface
{
	
	public function getUrl();
	
	public function getDatos();

	public function getParams();
	
	public function schemaValidate();
	
}