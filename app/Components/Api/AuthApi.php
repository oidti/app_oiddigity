<?php
namespace App\Components\Api;

use Illuminate\Support\Facades\Storage;
use App\Components\Api\AccesoApi;
use App\Components\Api\Exceptions\InternalApiException;
use App\Components\Api\Exceptions\UnauthorizedException;
use Carbon\Carbon;


class AuthApi extends AccesoApi
{
	
	private $url = 'ApiAuth/autenticacion';

	/*
	* Proceso gestion token.
	* Si no existe archivo o el token que hay ha caducado, llama a api autentificacion y guarda el token obtenido
	* Despues lee del archivo el token guardado y lo devuelve
	* Retorna objeto
	* 
	*/
	public function getAuthToken()
	{		
		if(!$this->isValidToken())
		{	
			if(!$this->conectApiAndSaveToken())
			{
				throw new UnauthorizedException();
			}
		}
		return $this->getFileAuth();
	}	

	/**
	* Si hay datos en el archivo autentificacion comprueba si el token sigue vigente
	* Si no hay archivo o ha caducado el token retorna false
	**/
	private function isValidToken()
	{		
		if(!empty($auth = $this->getFileAuth()))
		{			
			$current = Carbon::now();
			$caducidad = Carbon::parse($auth->caducidad);		
			//Si fecha actual es mayor que caducidad
			if($current->gt($caducidad)){
				return false;
			} else{
				return true;
			}
		}
		return false;
	}

	/**
	* Llama a Api autentificacion y guarda el token recibido en el archivo
	**/
	private function conectApiAndSaveToken()
	{				
		$this->conectApi($this->url, $this->getParams(), 'GET');
		

		if($this->validateSchemaResponseBody($this->schemaValidate()))
		{
			$this->saveAuth($this->respuestaApi);	
			return true;
		}			
		
		return false;
	}

	/**
	* Si existe archivo autentificacion obtiene los datos
	* Return object
	*/
	private function getFileAuth()
	{
		if($this->existFileAuth())
		{
			$obj = json_decode(Storage::get('file_auth.json'));
			if(is_object($obj))
				return $obj;
		}
		return false;
	}

	private function existFileAuth()
	{
		return Storage::exists('file_auth.json');
	}

	/**
	* Retorna los parametros de la peticion
	**/
	private function getParams()
	{
		return [
			    'query' => [
			    	'keyapp' => config('api.key_auth')
			    ]
			];

	}	

	/**
	* Crea el esquema para validar el body de la respuesta
	* Retorna array
	*/	
	private function schemaValidate()
	{
		return [
    		'token' => 'required|string',
    		'caducidad' => 'required|date|date_format:Y-m-d H:i:s'
		];
	}	
	
	/**
	* Guarda el token valido en el archivo
	*/
	private function saveAuth(array $auth)
	{
		Storage::put('file_auth.json', json_encode($auth));
	}	
	
	
	
	

	
	
}
?>