<?php
namespace App\Components\Api;

use App\Components\Api\AccesoApi;
use App\Components\Api\Exceptions\InternalApiException;
use Carbon\Carbon;
use Illuminate\Validation\Rule;

class BoteApi extends AccesoApi implements AccessClientInterface
{
	protected $date_ini;
	protected $date_fin;	
	protected $url;

	public function __construct(Carbon $d_ini, Carbon $d_fin)
	{

		$this->date_ini = $d_ini->format('Y-m-d');
		$this->date_fin = $d_fin->format('Y-m-d');			
		$this->url = $this->getUrl();

	}
	/*
	* Retorna la url completa con el token de seguridad
	*/
	public function getUrl()
	{
		return 'ApiBoteV2/consulta';
	}
	/*
	* Establece conexio con la Api, valida los datos recibidos segun el esquema de validacion
	* Si todo es correcto retorna los datos
	* Si no retorna false
	*/
	public function getDatos()
	{

		if($this->conectApi($this->url, $this->getParams(),'GET'))
		{			
			if($this->validateSchemaResponseBody($this->schemaValidate()))
			{
				return $this->respuestaApi;
			}

		}
		return false;

	}
	/*
	* Retorna los parametros necesarios para enviar a la Api
	*/
	public function getParams()
	{
		return [
			    'query' => [
			    	'token' => $this->getValidToken(),
					'date_ini' => $this->date_ini,
					'date_end' => $this->date_fin								
			]
		];
		
	}
	/*
	* Retorna las reglas de validacion que debe cumplir el body de la respuesta
	*/
	public function schemaValidate()
	{
		return [

			'botes' => 'required|array',

			'botes.*.date' => 'required|date',
			'botes.*.amount' => 'required|numeric',
			'botes.*.reserva' => 'required|numeric'
		];
	}
}
?>