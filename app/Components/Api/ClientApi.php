<?php
namespace App\Components\Api;

use Validator;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\Components\Api\Exceptions\GuzzleConectionException;
use App\Components\Api\Exceptions\UnauthorizedException;
use App\Components\Api\Exceptions\InternalApiException;
use App\Components\Api\Exceptions\BadRequestException;
use App\Components\Api\Exceptions\NotFoundException;
use Illuminate\Support\Facades\Log;

class ClientApi
{	
	private $response; // objeto response de la llamada a Api
	private $method;
	private $url;
	private $params;
	private $base_uri;
	
	
	/*
	* Configuracion parametros llamada api
	*/
	public function __construct(string $url, array $params, string $method)
	{
		
		$this->method = $method;
		$this->url = $url;
		$this->params = $params;
		$this->base_uri = config('api.base_url');
	}

	/**
	* Lanza conexion a la url configurada y retorna respuesta
	**/
	public function request()
	{
		try {

			$client = new Client(['base_uri' => $this->base_uri,'http_errors' => false, 'exceptions' => false ]);	

			$this->response = $client->request($this->method, $this->url, $this->params, ['read_timeout' => 25]); 			

			Log::info('Llamada API: '.$this->url, [
                'type' => $this->method,
                'request' => [
                    'params' => $this->params
                ],
                'response' => [
                    'code' => $this->response->getStatusCode(),
                    'body' => $this->response->getBody()->getContents()
                ]
            ]);
			
			//Valido codigo estado y formato respuesta (JSON)
			if($this->verifyStatusCode() === true && $this->isValidJsonResponseBody()){
				
				return json_decode($this->response->getBody(),true); //array				
				
			}

		} catch ( RequestException $e) {	
			
			throw new GuzzleConectionException($e->getMessage(), 500);

		} catch (ClientException $e) {	

            throw new GuzzleConectionException($e->getMessage(), 500);

        } catch (RequestException  $e) {

            throw new GuzzleConectionException($e->getMessage(), 500);

        }
		
	}
	
	/**
	* Comprobar que el cuerpo de la respuesta es un json valido
	*/
	private function isValidJsonResponseBody(){
		
		$result = json_decode($this->response->getBody());


		if (json_last_error() === JSON_ERROR_NONE) {
		    return true;
		}
		
		throw new InternalApiException("La respuesta no contiene un json válido");
	}

	/**
	* Comprobar el codigo estado recibido por el Api
	*/
	private function verifyStatusCode(){

		$msg = $this->response->getReasonPhrase();		

		switch ($this->response->getStatusCode()) {
            case 200:
                return true;
            case 202:
                throw new Exceptions\RequestIncompleteException($msg, 202);
            case 400:
                throw new BadRequestException($msg, 400);
            case 401:
                throw new UnauthorizedException($msg, 401);
            case 403:
                throw new UnauthorizedException($msg, 403);
            case 404:
                throw new NotFoundException($msg, 404);
            case 500:
                throw new InternalApiException($msg);
            default:                
                throw new InternalApiException("Error api desconocido");
                
        }

	}	
}
?>