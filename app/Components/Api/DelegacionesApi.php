<?php
namespace App\Components\Api;

class DelegacionesApi extends AccesoApi implements AccessClientInterface
{
	private $url = 'ApiInformes/listDelegaciones';
	
	public function getUrl()
	{
		return $this->url;
	}
	
	public function getDatos()
	{
		if($this->conectApi($this->url, $this->getParams(), 'GET'))
		{
			
			if($this->validateSchemaResponseBody($this->schemaValidate()))
			{
				
				return $this->respuestaApi['delegaciones'];
			}

		}
		return false;

	}
	/*
	* Retorna los parametros necesarios para enviar a la Api
	*/
	public function getParams()
	{

		return [
			'query' =>[
				'token' => $this->getValidToken()
			]		
		];
	}
	/*
	* Retorna las reglas de validacion que debe cumplir el body de la respuesta
	*/
	public function schemaValidate()
	{
		return [
			'delegaciones' => 'required|array'
		];
	}

}
?>