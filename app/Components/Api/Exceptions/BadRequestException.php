<?php
namespace App\Components\Api\Exceptions;

use Exception;

class BadRequestException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        //echo $this->getMessage();
        return response()->view('Errors.400');
    }
}
?>