<?php

namespace App\Components\Api\Exceptions;

use Exception;

/**
 * Description of InternalApiErrorException
 */
class InternalApiException extends Exception
{
    /**
     * Retorna la respuesta que debe mostrarse 
     * en caso de que suceda la excepción
     * 
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        //echo $this->getMessage();
        return response()->view('errors.500', [], 500);
    }
}