<?php
namespace App\Components\Api;

use App\Components\Api\AccesoApi;
use App\Components\Api\Exceptions\InternalApiException;
use Carbon\Carbon;
use Illuminate\Validation\Rule;

class RecaudacionDelegacionApi extends AccesoApi implements AccessClientInterface
{
	protected $date_ini;
	protected $date_fin;
	protected $delegacion_id;
	protected $filtro_bote;
	protected $url;

	public function __construct(Carbon $d_ini, Carbon $d_fin, int $delegacion_id = null, int $filtro_bote = -1)
	{

		$this->date_ini = $d_ini->format('Y-m-d');
		$this->d_fin = $d_fin->format('Y-m-d');
		$this->delegacion_id = $delegacion_id;		
		$this->url = $this->getUrl();

	}
	/*
	* Retorna la url completa con el token de seguridad
	*/
	public function getUrl()
	{
		return 'ApiInformes/recaudacionDelegacion?token='.$this->getValidToken();
	}
	/*
	* Establece conexio con la Api, valida los datos recibidos segun el esquema de validacion
	* Si todo es correcto retorna los datos
	* Si no retorna false
	*/
	public function getDatos()
	{

		if($this->conectApi($this->url, $this->getParams(),'POST'))
		{			
			if($this->validateSchemaResponseBody($this->schemaValidate()))
			{
				return $this->respuestaApi;
			}

		}
		return false;

	}
	/*
	* Retorna los parametros necesarios para enviar a la Api
	*/
	public function getParams()
	{
		return [
			    'form_params' => [
					'fecha_ini' => $this->date_ini,
					'fecha_fin' => $this->d_fin,
					'delegacion_id' => $this->delegacion_id					
			]
		];
		
	}
	/*
	* Retorna las reglas de validacion que debe cumplir el body de la respuesta
	*/
	public function schemaValidate()
	{
		return [

			'cad_entregados' => 'required|numeric',
			'cad_devueltos' => 'required|numeric',
			'cad_vendidos' => 'required|numeric',
			'cad_compensacion' => 'required|numeric',
			'cad_vend_pre1_cant' => 'required|numeric',
			'cad_vend_pre1' => 'required|numeric',
			'cad_vend_pre1y5_cant' => 'required|numeric',
			'cad_vend_pre1y5' => 'required|numeric',
			'cad_vend_pre1y5' => 'required|numeric',
			'cad_vend_pre2_cant' => 'required|numeric',
			'cad_vend_pre3_cant' => 'required|numeric',
			'cad_vend_pre3' => 'required|numeric',
			'cad_vend_pre5_cant' => 'required|numeric',
			'cad_vend_pre5' => 'required|numeric',
			'cad_vend_pre10_cant' => 'required|numeric',
			'cad_vend_pre10' => 'required|numeric',
			'cad_vend_pre30_cant' => 'required|numeric',
			'cad_vend_pre30' => 'required|numeric',
			'cad_vend_pre50_cant' => 'required|numeric',
			'cad_vend_pre50' => 'required|numeric',
			'cad_vend_pre60_cant' => 'required|numeric',
			'cad_vend_pre60' => 'required|numeric',
			'cad_vend_pre100_cant' => 'required|numeric',
			'cad_vend_pre100' => 'required|numeric',
			'cad_vend_pre300_cant' => 'required|numeric',
			'cad_vend_pre300' => 'required|numeric',
			'cad_vend_pre500_cant' => 'required|numeric',
			'cad_vend_pre500' => 'required|numeric',
			'cad_vend_pre600_cant' => 'required|numeric',
			'cad_vend_pre600' => 'required|numeric',
			'cad_vend_pre1000_cant' => 'required|numeric',
			'cad_vend_pre1000' => 'required|numeric',

			'cad_total_insp_pre1_cant' => 'required|numeric',
			'cad_total_insp_pre1' => 'required|numeric',
			'cad_total_insp_pre1y5_cant' => 'required|numeric',
			'cad_total_insp_pre1y5' => 'required|numeric',
			'cad_total_insp_pre2_cant' => 'required|numeric',
			'cad_total_insp_pre2' => 'required|numeric',
			'cad_total_insp_pre3_cant' => 'required|numeric',
			'cad_total_insp_pre3' => 'required|numeric',
			'cad_total_insp_pre5_cant' => 'required|numeric',
			'cad_total_insp_pre5' => 'required|numeric',
			'cad_total_insp_pre10_cant' => 'required|numeric',
			'cad_total_insp_pre10' => 'required|numeric',
			'cad_total_insp_pre30_cant' => 'required|numeric',
			'cad_total_insp_pre30' => 'required|numeric',
			'cad_total_insp_pre50_cant' => 'required|numeric',
			'cad_total_insp_pre50' => 'required|numeric',
			'cad_total_insp_pre60_cant' => 'required|numeric',
			'cad_total_insp_pre60' => 'required|numeric',
			'cad_total_insp_pre100_cant' => 'required|numeric',
			'cad_total_insp_pre100' => 'required|numeric',
			'cad_total_insp_pre300_cant' => 'required|numeric',
			'cad_total_insp_pre300' => 'required|numeric',
			'cad_total_insp_pre500_cant' => 'required|numeric',
			'cad_total_insp_pre500' => 'required|numeric',
			'cad_total_insp_pre600_cant' => 'required|numeric',
			'cad_total_insp_pre600' => 'required|numeric',
			'cad_total_insp_pre1000_cant' => 'required|numeric',
			'cad_total_insp_pre1000' => 'required|numeric',

			'cad_facturas' => 'required|numeric',
			'cad_nominas' => 'required|numeric',
			'cad_gastos' => 'required|numeric',
			'cad_suma_anticipos' => 'required|numeric',
			'cad_suma_deudas' => 'required|numeric',
			'cad_suma_devolucion_anticipo' => 'required|numeric',
			'cad_suma_pago_deuda' => 'required|numeric',
			'cad_suma_pago_devoluciones' => 'required|numeric',
			'cad_sumas' => 'required|numeric',
			'cad_restas' => 'required|numeric',
			'cad_suma_ingresado' => 'required|numeric',
			'cad_saldo' => 'required|numeric',
			'cad_delegacion_id' => 'required|numeric',
			'delegacion' => 'required|string'
			
		];
	}
}
?>