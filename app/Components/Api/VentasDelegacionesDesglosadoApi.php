<?php
namespace App\Components\Api;

use App\Components\Api\BaseApi;
use App\Components\Api\Exceptions\InternalApiException;
use Carbon\Carbon;
use Illuminate\Validation\Rule;

class VentasDelegacionesDesglosadoApi extends AccesoApi implements AccessClientInterface
{
	protected $date_ini;
	protected $date_fin;
	protected $delegacion_id;
	protected $filtro_bote;
	protected $url;

	public function __construct(Carbon $d_ini, Carbon $d_fin, int $delegacion_id = null, int $filtro_bote = -1)
	{

		$this->date_ini = $d_ini->format('Y-m-d');
		$this->d_fin = $d_fin->format('Y-m-d');
		$this->delegacion_id = $delegacion_id;
		$this->filtro_bote = $filtro_bote;
		$this->url = $this->getUrl();

	}
	/*
	* Retorna la url completa con el token de seguridad
	*/
	public function getUrl()
	{
		return 'ApiInformes/ventasDelegacionesDesglosado?token='.$this->getValidToken();
	}
	/*
	* Establece conexio con la Api, valida los datos recibidos segun el esquema de validacion
	* Si todo es correcto retorna los datos
	* Si no retorna false
	*/
	public function getDatos(){

		if($this->conectApi($this->url, $this->getParams(),'POST'))
		{					
			if($this->validateSchemaResponseBody($this->schemaValidate()))
			{
				return $this->respuestaApi;
			}

		}
		return false;

	}
	/*
	* Retorna los parametros necesarios para enviar a la Api
	*/
	public function getParams()
	{
		return [
			    'form_params' => [
					'fecha_ini' => $this->date_ini,
					'fecha_fin' => $this->d_fin,
					'delegacion_id' => $this->delegacion_id,
					'filtro_bote' => $this->filtro_bote
			]
		];
		
	}
	/*
	* Retorna las reglas de validacion que debe cumplir el body de la respuesta
	*/
	public function schemaValidate()
	{
		return [
			'datos_venta' => 'required|array',
			'datos_venta.*.nom_deleg' => 'required|string',
			'datos_venta.*.entregados.boletos' => 'required|numeric',
			'datos_venta.*.entregados.euros' => 'required|numeric',
			'datos_venta.*.devolucion.boletos' => 'required|numeric',
			'datos_venta.*.devolucion.euros' => 'required|numeric',
			'datos_venta.*.ventas.boletos' => 'required|numeric',
			'datos_venta.*.ventas.euros' => 'required|numeric',
			'datos_venta.*.sucursales' => 'present|array',			
			'totales' => 'required|array',
			'totales.asig.euros' => 'required|numeric',
			'totales.asig.boletos' => 'required|numeric',
			'totales.dev.euros' => 'required|numeric',
			'totales.dev.boletos' => 'required|numeric',
			'totales.vend.euros' => 'required|numeric',
			'totales.vend.boletos' => 'required|numeric',
			'sorteo' => 'required|string'
		];
	}
}
?>
