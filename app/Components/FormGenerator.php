<?php
namespace App\Components;

use App\Components\Api\DelegacionesApi;
use Collective\Html\FormFacade;
 
class FormGenerator
{
 	/**
     * Genera select con las delegaciones
     *
     * @param  string $name
     * @param  array  $allOptions
     * @param  string $selected
     * @param  array  $selectAttributes
     * @param  array  $optionsAttributes
     *
     * 
     */
    public function selectDelegaciones($name, $allOptions = null, $selected = null, array $selectAttributes = [], array $optionsAttributes = [])
    {
    	$delegaciones = new DelegacionesApi();
    	$options = $delegaciones->getDatos(); 
        
    	
    	if( $allOptions === 1 )
    	{
    		$options = array_prepend($options, 'Seleccionar todas',0);
    	}
    	return FormFacade::select($name,$options, null, $selectAttributes, $optionsAttributes);
    }
}
?>