<?php
namespace App\Facades;
use Illuminate\Support\Facades\Facade;
 
class FormGenerator extends Facade {
 
    protected static function getFacadeAccessor()
    {
        return 'build';
    }
}
?>