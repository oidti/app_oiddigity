<?php

namespace App\Http\Controllers\Bote;


use App\Http\Controllers\Controller;
use App\Components\Api\BoteApi;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;


class ListadoBotesController extends Controller
{
	/**
	* Nombre de la ruta donde se muestra el informe
	**/
	protected $route_informe = 'botes.informe';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
    * Muestra el formulario para hacer la busqueda
    **/
    public function index()
    {	
        return view('forms.filterRangeDate',[
            'name_route'=>$this->route_informe,
            'title' => 'Selección fechas bote',
           
        ]);
    }

    /**
    * Muestra el informe segun los datos recibidos del formulario
    **/
    public function showInforme(request $request)
    {
       
    	ini_set('max_execution_time', 0);   	    

        $validated = $request->validate([
            'fecha_inicio' => 'required|date',
            'fecha_fin' => 'required|date',
        ]);    

    	$fecha_ini = Carbon::createFromFormat('d-m-Y',$validated['fecha_inicio']);
        $fecha_fin = Carbon::createFromFormat('d-m-Y',$validated['fecha_fin']);  
       
        $data = new BoteApi($fecha_ini, $fecha_fin);

        $informe = $data->getDatos();        

        return view('informes.botes',[
            'campos' => $informe,                       
            'title' => 'Listado botes',
            'route_return' => 'botes', 
            'params' => [
                'f_ini' => $fecha_ini->format('d-m-Y'),
                'f_fin' => $fecha_fin->format('d-m-Y')                
            ]            
        ]);
     
    } 

}
