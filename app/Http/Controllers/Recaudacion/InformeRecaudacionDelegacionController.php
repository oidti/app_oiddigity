<?php

namespace App\Http\Controllers\Recaudacion;


use App\Http\Controllers\Controller;
use App\Http\Requests\ValidRangeDateAndDelegacion;
use App\Components\Api\RecaudacionDelegacionApi;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;


class InformeRecaudacionDelegacionController extends Controller
{
	/**
	* Nombre de la ruta donde se muestra el informe
	**/
	protected $route_informe = 'recaudacion.delegacion.informe';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
    * Muestra el formulario para hacer la busqueda
    **/
    public function index()
    {	
        return view('forms.filterRangeDateAndDelegacion',[
            'name_route'=>$this->route_informe,
            'title' => 'Selección recaudacion delegaciones',
           
        ]);
    }

    /**
    * Muestra el informe segun los datos recibidos del formulario
    **/
    public function showInforme(ValidRangeDateAndDelegacion $request)
    {
       
    	ini_set('max_execution_time', 0);   	        

    	$fecha_ini = Carbon::createFromFormat('d-m-Y',$request->fecha_inicio);
        $fecha_fin = Carbon::createFromFormat('d-m-Y',$request->fecha_fin);  
        $delegacion_id = $request->delegacion_id;
        
        $data = new RecaudacionDelegacionApi($fecha_ini, $fecha_fin, $request->delegacion_id);

        $informe = $data->getDatos();        

        return view('informes.recaudacion',[
            'campos' => $informe,                       
            'title' => 'Informe recaudación delegación',
            'route_return' => 'recaudacion.delegacion', 
            'params' => [
                'f_ini' => $fecha_ini->format('d-m-Y'),
                'f_fin' => $fecha_fin->format('d-m-Y')                
            ]            
        ]);
     
    } 

}
