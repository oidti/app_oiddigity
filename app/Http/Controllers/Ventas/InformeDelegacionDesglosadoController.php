<?php

namespace App\Http\Controllers\Ventas;


use App\Http\Controllers\Controller;
use App\Http\Requests\ValidRangeDate;
use App\Components\Api\VentasDelegacionesDesglosadoApi;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;



class InformeDelegacionDesglosadoController extends Controller
{
	/**
	* Nombre de la ruta donde se muestra el informe
	**/
	protected $route_informe = 'ventas.delegacion.desglosado.informe';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');      

    }

    /**
    * Muestra el formulario para hacer la busqueda
    **/
    public function index()
    {     	
        return view('forms.filtroDelegacion',[
            'name_route'=>$this->route_informe,
            'title' => 'Selección ventas delegacion desglosado'        
        ]);
    }

    /**
    * Muestra el informe segun los datos recibidos del formulario
    **/
    public function showInforme(ValidRangeDate $request)
    {
    	ini_set('max_execution_time', 0);   	
        //$validatedData = $request->validated();

    	$fecha_ini = Carbon::createFromFormat('d-m-Y',$request->fecha_inicio);
        $fecha_fin = Carbon::createFromFormat('d-m-Y',$request->fecha_fin);     
        $key = 'ventasDelegacionDesglosado_'. $fecha_ini->format('Y-m-d') .'_'. $fecha_fin->format('Y-m-d') .'_'. $request->delegacion_id .'_'. $request->filtro_bote .'_'. $request->delegacion_id;         
        if(!Cache::has($key))
        {
            $data = new VentasDelegacionesDesglosadoApi($fecha_ini, $fecha_fin, $request->delegacion_id, $request->filtro_bote);
            $informe = $data->getDatos(); 

            array_multisort($informe['datos_venta']);

            Cache::put($key,$informe,60);

        }
        $informe = Cache::get($key);    
      
        return view('informes.ventasDesglosado',[
            'ventas' => $informe,                       
            'title' => 'Informe ventas delegaciones desglosado',
            'route_return' => 'ventas.delegacion.desglosado',
            'params' => [
                'f_ini' => $fecha_ini->format('d-m-Y'),
                'f_fin' => $fecha_fin->format('d-m-Y'),
                'sorteo' => $informe['sorteo']
            ]            
        ]);
    }    
}
