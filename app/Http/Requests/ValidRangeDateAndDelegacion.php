<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/*
* Valida las entradas de formulario para informes por rango de fechas y id delegacion (obligatorios)
*/

class ValidRangeDateAndDelegacion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fecha_inicio' => 'required|date|date_format:d-m-Y',
            'fecha_fin' => 'required|date|date_format:d-m-Y',
            'delegacion_id' => 'required|integer'
        ];  
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'fecha_inicio.required' => 'Fecha de inicio obligatoria',
            'fecha_fin.required' => 'Fecha fin obligatoria',
            'delegacion_id' => 'Debe seleccionar una delegación'
        ];
    }
}
