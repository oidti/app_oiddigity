<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'nombre' => 'Estefania',
            'apellidos' => 'Sanchez Mayoral',            
            'username' => 'essanchez',
            'email' => 'esanchez@gruposoid.com',
            'password' => bcrypt('d3s13rt0')            
        ]);
    }
}
