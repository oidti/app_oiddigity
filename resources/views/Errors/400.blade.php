@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="error-template">
                <h1>
                    Oops!</h1>
                <h2>
                    400 Bad request</h2>
                <div class="error-details">
                    El servidor no puede procesar el request por un error de sintaxis del cliente.
                </div>
                <div class="error-actions">
                    <a href="{{ url('/') }}"><span class="glyphicon glyphicon-home"></span>
                        Ir al inicio </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection