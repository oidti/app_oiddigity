
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="error-template">
                <h1>
                    Oops!</h1>
                <h2>
                    500 Internal Server Error</h2>
                <div class="error-details">
                    Fallo en la conexion con el servidor. Pulse el botón para volver al inicio
                </div>
                <div class="error-actions">
                    <a href="{{ url('/') }}"><span class="glyphicon glyphicon-home"></span>
                        Ir al inicio </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection