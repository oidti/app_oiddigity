@extends('layouts.app')

@section('content')

<div class="container-fluid ">
    <div class="card card-container">
        <h2 class='login_title text-center'>Iniciar sesión</h2>
        <hr>
        <form class="form-signin" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

            <p class="input_title">Usuario</p>
            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
            @if ($errors->has('username'))
                <span class="text-danger">
                   {{ $errors->first('username') }}
                </span>
            @endif
            <p class="input_title">Contraseña</p>
            <input id="password" type="password" class="form-control" name="password" required>
            @if ($errors->has('password'))
                <span class="text-danger">
                    {{ $errors->first('password') }}
                </span>
            @endif
            <div id="remember" class="checkbox">
                <label> </label>
            </div>
            <button class="btn btn-lg btn-primary" type="submit">Entrar</button>
        </form>
        
    </div>  
     
</div>

@endsection
