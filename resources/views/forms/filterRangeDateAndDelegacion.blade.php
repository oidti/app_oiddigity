<!--
**	
* Vista de formulario para filtrar informes por rango de fechas y delegacion (campos obligatorios)
**
-->
@extends('layouts.app')

@section('content')	


<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
	                <div class="panel-heading">{{ $title }}</div>

	                <div class="panel-body">
	                	{!! Form::open( array('route' => $name_route,'class' => 'form-horizontal','id' => 'filtros')) !!}
						
				    	<div class="form-group">
				    		<!-- Fecha inicio -->
				    		<div class="col-xs-12 col-sm-2">
				    			{!! Form::label('fecha_inicio', 'Fecha inicio', array('class' => 'control-label')) !!}
				    		</div>
				    		<div class="col-xs-12 col-sm-4">
				    			{!! Form::text('fecha_inicio',null, ['class' => 'form-control datepicker']) !!}
				    			@if ($errors->has('fecha_inicio'))
                                    <span class="text text-danger">
                                        <strong>{{ $errors->first('fecha_inicio') }}</strong>
                                    </span>
                                @endif
				    		</div>
				    		<!-- Fecha fin -->
				    		<div class="col-xs-12 col-sm-2">
				    			{!! Form::label('fecha_fin', 'Fecha fin', array('class' => 'control-label')) !!}
				    		</div>
				    		<div class="col-xs-12 col-sm-4">
				    			{!! Form::text('fecha_fin',null, ['class' => 'form-control datepicker']) !!}
				    			@if ($errors->has('fecha_fin'))
                                    <span class="text text-danger">
                                        <strong>{{ $errors->first('fecha_fin') }}</strong>
                                    </span>
                                @endif
				    		</div>
				    		
				    	</div>
				    	<div class="form-group">
				    		<!-- Select delegacion-->
				    		<div class="col-xs-12 col-sm-2">
				    			{!! Form::label('delegacion_id', 'Delegacion (obligatorio)', array('class' => 'control-label')) !!}
				    		</div>
				    		<div class="col-xs-12 col-sm-4">
				    			{!! Build::selectDelegaciones('delegacion_id', 0, null, array('class' => 'form-control')) !!}				    			
				    		</div>
				    		
				    	</div>
				    	<div class="form-group">
				            <div class="col-xs-12">
				                <button  id="btn-form" type="submit" class="btn btn-primary btn-block" style="display: block;">
				                    Enviar
				                </button>	
				                <!-- Gif enviando -->
				                {{ Html::image('images/cargar_azul.gif', 'alt text',array('id' => 'loading','style' => 'display: none;','class' => 'img-center')) }}
				            </div>
	                    </div>
	    				{{ Form::close() }}
	                </div>
	            </div>
	        </div>    
   		</div>
   	</div>
</div>

@endsection

