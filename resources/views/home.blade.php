@extends('layouts.app')


@section('content')

<div class="container-fluid">   

    <div class="jumbotron">
        <h2><i class="entypo-check"></i>App OID</h2>
        <h4 class="text-muted">Aplicación de acceso a informes</h4>
        <p class="text text-info">Selecciona en el menu superior el informe a consultar.</p>
    </div>

   
            <p><strong>Accesos directos a:</strong></p>
            <p><a href='{{ route('ventas.delegacion.desglosado') }}'><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Ventas delegaciones desglosado</a></p>
            <p><a href='{{ route('ventas.delegacion') }}'><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Ventas delegaciones </a></p>
       
    </div>

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

</div>
@endsection


