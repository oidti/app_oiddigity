@extends('layouts.app')

@section('content')

<div class="container-fluid">
	
	<div class="row">	
	 <div class="col-xs-12 margen-bottom">
		{!! Html::linkRoute($route_return, 'Cambiar selección', array(), array('class' => 'btn btn-primary')) !!}		
		</div>			
	</div>
	<div class="container-shadow">
    <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title">
    			<h3>{{ $title }}</h3><br>
    			<strong>Fechas: </strong>
    				Del {{ $params['f_ini'] }} al {{ $params['f_fin'] }}<br>    				

    		</div>    		
    		
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-xs-12">    		
    		<table class="table table-min table-responsive table-bordered table-striped table-hover">
				<tr>
				    <th>Fecha</th>
				    <th>Cantidad bote</th>
				    <th>Reserva</th>				    
				</tr>
				
				@forelse ($campos['botes'] as $reg)					
					<tr>
						<td>{{ $reg['date'] }}</td>
						<td>{{ $reg['amount'] }} €</td>
						<td>{{ $reg['reserva']}} €</td>
						
					</tr>
				@empty
				    <td>No hay datos que mostrar</td>
				@endforelse	
				
			</table>    			
    	</div>
    </div>
    </div>
</div>
@endsection


