@extends('layouts.app')

@section('content')

<div class="container-fluid ">
    
	<div class="row">	
	 <div class="col-xs-12 margen-bottom">
		{!! Html::linkRoute($route_return, 'Cambiar selección', array(), array('class' => 'btn btn-primary')) !!}		
		</div>			
	</div>
    <div class="container-shadow">
        <div class="row">
            <div class="col-xs-12">
        		<div class="invoice-title">
        			<h3>{{ $title }}</h3><br>
        			<strong>Fechas: </strong>
        				Del {{ $params['f_ini'] }} al {{ $params['f_fin'] }}<br>
        				<strong>Delegacion: </strong>
        				{{ $campos['delegacion'] }}

        		</div>    		
        		
        	</div>
        </div>
      

                <div class="row caja_bordes caja_cabecera margen">
                	 

            		            <div class="col-xs-6 texto_centrado texto_cabecera_recaudacion caja_cabecera_destacado">
            		                {{ $campos['cad_entregados'] }}
            		            </div>
            		            <div class="col-xs-6 texto_izquierda texto_cabecera_recaudacion caja_cabecera_destacado">
            		                ENTREGADOS
            		            </div>
            		            
            		            <div class="col-xs-6 texto_centrado texto_cabecera_recaudacion caja_cabecera_destacado">
            		                {{ $campos['cad_devueltos'] }}   
            		            </div>
            		            <div class="col-xs-6 texto_izquierda texto_cabecera_recaudacion caja_cabecera_destacado">
            		                DEVUELTOS
            		            </div>
            		            
            		            <div class="col-xs-6 texto_centrado texto_cabecera_recaudacion caja_cabecera_destacado">
            		               {{ $campos['cad_vendidos'] }}

            		            </div>
            		            <div class="col-xs-6 texto_izquierda texto_cabecera_recaudacion caja_cabecera_destacado">
            		                VENDIDOS
            		            </div>
            		       
            		    
                </div>
                <div class="row texto_negro">
                    <div class="col-xs-6 texto_centrado texto_cabecera_recaudacion">
                        {{ $campos['cad_compensacion'] }}       
                    </div>
                    <div class="col-xs-6 texto_izquierda texto_cabecera_recaudacion">
                            Compensación de Gastos
                        </div>
                </div>
        
                <table class="table table-sm font-mobile">
                    <thead class="thead-light "><tr><th colspan="6" class="borde_top_bottom">PREMIOS VENDEDOR </th></thead>
                    <tbody>
                    <tr class="borde-bottom"><td> {{ $campos['cad_vend_pre1_cant'] }}</td><td> x 1,00€ = </td><td class="borde-right">{{ $campos['cad_vend_pre1'] }}</td> 
                        <td> {{ $campos['cad_vend_pre50_cant'] }}</td><td> x 50,00€ = </td><td>{{ $campos['cad_vend_pre50'] }}</td>
                    </tr>
                    <tr class="borde-bottom"><td> {{ $campos['cad_vend_pre1y5_cant'] }}</td><td> x1,50€ = </td><td class="borde-right">{{ $campos['cad_vend_pre1y5'] }}</td> 
                        <td> {{ $campos['cad_vend_pre60_cant'] }}</td><td> x 60,00€ = </td><td>{{ $campos['cad_vend_pre60'] }}</td>
                    </tr>
                    <tr class="borde-bottom"><td> {{ $campos['cad_vend_pre2_cant'] }}</td><td> x 2,00€ = </td><td class="borde-right">{{ $campos['cad_vend_pre2'] }}</td> 
                        <td> {{ $campos['cad_vend_pre80_cant'] }}</td><td> x 80,00€ = </td><td>{{ $campos['cad_vend_pre80'] }}</td>
                    </tr>

                    <tr class="borde-bottom"><td> {{ $campos['cad_vend_pre3_cant'] }}</td><td> x 3,00€ = </td><td class="borde-right">{{ $campos['cad_vend_pre3'] }}</td> 
                        <td> {{ $campos['cad_vend_pre100_cant'] }}</td><td> x 100,00€ = </td><td>{{ $campos['cad_vend_pre100'] }}</td>
                    </tr>
                    <tr class="borde-bottom"><td> {{ $campos['cad_vend_pre5_cant'] }}</td><td> x5,00€ = </td><td class="borde-right">{{ $campos['cad_vend_pre5'] }}</td> 
                        <td> {{ $campos['cad_vend_pre300_cant'] }}</td><td> x 300,00€ = </td><td>{{ $campos['cad_vend_pre300'] }}</td>
                    </tr>
                    <tr class="borde-bottom"><td> {{ $campos['cad_vend_pre6_cant'] }}</td><td> x 6,00€ = </td><td class="borde-right">{{ $campos['cad_vend_pre6'] }}</td> 
                        <td> {{ $campos['cad_vend_pre500_cant'] }}</td><td> x 500,00€ = </td><td>{{ $campos['cad_vend_pre500'] }}</td>
                    </tr>

                    <tr class="borde-bottom"><td> {{ $campos['cad_vend_pre10_cant'] }}</td><td> x 10,00€ = </td><td class="borde-right">{{ $campos['cad_vend_pre10'] }}</td> 
                        <td> {{ $campos['cad_vend_pre600_cant'] }}</td><td> x 600,00€ = </td><td>{{ $campos['cad_vend_pre600'] }}</td>
                    </tr>
                    <tr class="borde-bottom"><td> {{ $campos['cad_vend_pre20_cant'] }}</td><td> x 20,00€ = </td><td class="borde-right">{{ $campos['cad_vend_pre20'] }}</td> 
                        <td> {{ $campos['cad_vend_pre800_cant'] }}</td><td> x 800,00€ = </td><td>{{ $campos['cad_vend_pre800'] }}</td>
                    </tr>
                    <tr class="borde-bottom"><td> {{ $campos['cad_vend_pre30_cant'] }}</td><td> x 30,00€ = </td><td class="borde-right">{{ $campos['cad_vend_pre30'] }}</td> 
                        <td> {{ $campos['cad_vend_pre1000_cant'] }}</td><td> x 1000,00€ = </td><td>{{ $campos['cad_vend_pre1000'] }}</td>
                    </tr>
                    </tbody>
                </table>    
            
        
                <table class="table table-sm  font-mobile">
                    <thead class="thead-light "><tr><th colspan="6" class="borde_top_bottom">PREMIOS INSPECTOR </th></thead>
                        <tbody>
                    <tr class="borde-bottom"><td> {{ $campos['cad_total_insp_pre1_cant'] }}</td><td> x 1,00€ = </td><td class="borde-right">{{ $campos['cad_total_insp_pre1'] }}</td> 
                        <td> {{ $campos['cad_total_insp_pre50_cant'] }}</td><td> x 50,00€ = </td><td>{{ $campos['cad_total_insp_pre50'] }}</td>
                    </tr>
                    <tr class="borde-bottom"><td> {{ $campos['cad_total_insp_pre1y5_cant'] }}</td><td> x 1,50€ = </td><td class="borde-right">{{ $campos['cad_total_insp_pre1y5'] }}</td> 
                        <td> {{ $campos['cad_total_insp_pre60_cant'] }}</td><td> x 60,00€ = </td><td>{{ $campos['cad_total_insp_pre60'] }}</td>
                    </tr>
                    <tr class="borde-bottom"><td> {{ $campos['cad_total_insp_pre2_cant'] }}</td><td> x 2,00€ = </td><td class="borde-right">{{ $campos['cad_total_insp_pre2'] }}</td> 
                        <td> {{ $campos['cad_total_insp_pre80_cant'] }}</td><td> x 80,00€ = </td><td>{{ $campos['cad_total_insp_pre80'] }}</td>
                    </tr>

                    <tr class="borde-bottom"><td> {{ $campos['cad_total_insp_pre3_cant'] }}</td><td> x 3,00€ = </td><td class="borde-right">{{ $campos['cad_total_insp_pre3'] }}</td> 
                        <td> {{ $campos['cad_total_insp_pre100_cant'] }}</td><td> x 100,00€ = </td><td>{{ $campos['cad_total_insp_pre100'] }}</td>
                    </tr>
                    <tr class="borde-bottom"><td> {{ $campos['cad_total_insp_pre5_cant'] }}</td><td> x 5,00€ = </td><td class="borde-right">{{ $campos['cad_total_insp_pre5'] }}</td> 
                        <td> {{ $campos['cad_total_insp_pre300_cant'] }}</td><td> x 300,00€ = </td><td>{{ $campos['cad_total_insp_pre300'] }}</td>
                    </tr>
                    <tr class="borde-bottom"><td> {{ $campos['cad_total_insp_pre6_cant'] }}</td><td> x 6,00€ = </td><td class="borde-right">{{ $campos['cad_total_insp_pre6'] }}</td> 
                        <td> {{ $campos['cad_total_insp_pre500_cant'] }}</td><td> x 500,00€ = </td><td>{{ $campos['cad_total_insp_pre500'] }}</td>
                    </tr>

                    <tr class="borde-bottom"><td> {{ $campos['cad_total_insp_pre10_cant'] }}</td><td> x 10,00€ = </td><td class="borde-right">{{ $campos['cad_total_insp_pre10'] }}</td> 
                        <td> {{ $campos['cad_total_insp_pre600_cant'] }}</td><td> x 600,00€ = </td><td>{{ $campos['cad_total_insp_pre600'] }}</td>
                    </tr>
                    <tr class="borde-bottom"><td> {{ $campos['cad_total_insp_pre20_cant'] }}</td><td> x 20,00€ = </td><td class="borde-right">{{ $campos['cad_total_insp_pre20'] }}</td> 
                        <td> {{ $campos['cad_total_insp_pre800_cant'] }}</td><td> x 800,00€ = </td><td>{{ $campos['cad_total_insp_pre800'] }}</td>
                    </tr>
                    <tr class="borde-bottom"><td> {{ $campos['cad_total_insp_pre30_cant'] }}</td><td> x 30,00€ = </td><td class="borde-right">{{ $campos['cad_total_insp_pre30'] }}</td> 
                        <td> {{ $campos['cad_total_insp_pre1000_cant'] }}</td><td> x 1000,00€ = </td><td>{{ $campos['cad_total_insp_pre1000'] }}</td>
                    </tr>
                    </tbody>
                </table>

                <table class="table table-sm" style="width: 100%">
                    <thead></thead>
                    <tbody>
                        <tr><td class="borde-top borde-right td-importes-totales">{{ number_format($campos['cad_facturas'], 2, ',', '.') }}</td><td class="negrita">Facturas</td></tr>
                        <tr><td class="borde-right td-importes-totales  borde-bottom">{{ number_format($campos['cad_nominas'], 2, ',', '.') }}</td><td class="negrita borde-bottom">Nóminas</td></tr>

                        <tr><td class=" borde-right td-importes-totales"> {{ number_format($campos['cad_vendidos'], 2, ',', '.') }}</td><td class="negrita">(A) VENDIDOS</td></tr>
                        <tr><td class="borde-right td-importes-totales">{{ number_format($campos['cad_gastos'], 2, ',', '.') }}</td><td class="negrita">(B) GASTOS</td></tr>

                         <tr><td class="borde-right td-importes-totales"> {{ number_format($campos['cad_suma_anticipos'], 2, ',', '.') }}</td><td class="negrita">(C) ANTICIPOS</td></tr>
                        <tr><td class="borde-right td-importes-totales">{{ number_format($campos['cad_suma_deudas'], 2, ',', '.') }}</td><td class="negrita"> (D) DEUDAS</td></tr>

                         <tr><td class="borde-right td-importes-totales"> {{ number_format($campos['cad_suma_devolucion_anticipo'], 2, ',', '.') }}</td><td class="negrita"> (E) DEVOLUCIÓN ANTICIPO</td></tr>
                        <tr><td class="borde-right td-importes-totales">{{ number_format($campos['cad_suma_pago_deuda'], 2, ',', '.') }}</td><td class="negrita"> (F) PAGO DEUDA</td></tr>


                         <tr><td class="borde-right td-importes-totales  borde-bottom"> {{ number_format($campos['cad_suma_pago_devoluciones'], 2, ',', '.') }}</td><td class="negrita  borde-bottom">  (G) PAGO DEVOLUCIONES</td></tr>

                        <tr><td class="borde-right td-importes-totales">{{ number_format($campos['cad_total_a_ingresar'], 2, ',', '.') }}</td>
                            <td class="borde-superior negrita">  (H) TOTAL A INGRESAR <span class="formula_recaudacion">(A)-(B)-(C)-(D)+(E)+(F)+(G)</span></td>
                        </tr>
                        <tr><td class="borde-right td-importes-totales  borde-bottom"> {{ number_format($campos['cad_suma_ingresado'], 2, ',', '.') }}</td><td class="negrita  borde-bottom">  (I) INGRESADO</td></tr>
                        <tr><td class="borde-right td-importes-totales  borde-bottom">{{ number_format($campos['cad_saldo'], 2, ',', '.') }}</td><td  class="borde-top negrita  borde-bottom"> (J) SALDO</td></tr>
                    </tbody>
                </table>
    </div>
</div>

@endsection