@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="row">	
	 <div class="col-xs-12 margen-bottom">
		{!! Html::linkRoute($route_return, 'Cambiar selección', array(), array('class' => 'btn btn-primary btn-icon icon-left','escape'=>false)) !!}		
		</div>			
	</div>
	<div class="container-shadow">
    <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title ">
    			<h3>{{ $title }}</h3><br>
    			<strong>Fechas: </strong>
    				Del {{ $params['f_ini'] }} al {{ $params['f_fin'] }}<br>
    				<strong>Sorteo: </strong>
    				{{ $params['sorteo'] }}

    		</div>    		
    		
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-xs-12">    		
    		<table class="table table-min table-responsive table-bordered table-striped table-hover">
				<tr>
				    <th>Delegación</th>
				    <th>Entregados</th>
				    <th>Devueltos</th>
				    <th>Vendidos</th>
				</tr>
				
				@forelse ($ventas['datos_venta'] as $reg)	
					<tr class="tr-resalt">
						<td>{{ strtoupper($reg['nom_deleg']) }}</td>
						<td>{{ $reg['entregados']['boletos'] }} bol. / {{ number_format($reg['entregados']['euros'],2,',','.') }} €</td>
						<td>{{ $reg['devolucion']['boletos'] }} bol. / {{ number_format($reg['devolucion']['euros'],2,',','.') }} €</td>
						<td>{{ $reg['ventas']['boletos'] }} bol. / {{ number_format($reg['ventas']['euros'],2,',','.') }} €</td>
					</tr>
					@foreach ($reg['sucursales'] as $suc)
						<tr>
							<td>{{ $suc['nom'] }}</td>
							<td>{{ $suc['entregados']['boletos'] }} bol. / {{ number_format($suc['entregados']['euros'],2,',','.') }} €</td>
							<td>{{ $suc['devolucion']['boletos'] }} bol. / {{ number_format($suc['devolucion']['euros'],2,',','.') }} €</td>
							<td>{{ $suc['ventas']['boletos'] }} bol. / {{ number_format($suc['ventas']['euros'],2,',','.') }} €</td>
						</tr>
					
					@endforeach
				@empty
				    <tr><td>No hay datos que mostrar</td></tr>
				@endforelse

				@if(isset($ventas['central']))
					<tr class="tr-resalt">
						<td>CENTRAL: </td>
						<td>{{ $ventas['central']['asignados']['boletos'] }} bol. / {{ number_format($ventas['central']['asignados']['euros'],2,',','.') }} €</td>
						<td>{{ $ventas['central']['devueltos_auto']['boletos'] }} bol. / {{ number_format( $ventas['central']['devueltos_auto']['euros'],2,',','.') }} €</td>
						<td>{{ $ventas['central']['vendido']['boletos'] }} bol. / {{ number_format($ventas['central']['vendido']['euros'],2,',','.') }} €</td>
					</tr>
				@endif	
				<tr class="inverse">
					<td><strong>TOTALES:</strong> </td>
					<td> {{ $ventas['totales']['asig']['boletos'] }} bol. / {{ number_format($ventas['totales']['asig']['euros'],2,',','.') }} €</td>
					<td> {{ $ventas['totales']['dev']['boletos'] }} bol. / {{ number_format($ventas['totales']['dev']['euros'],2,',','.') }} €</td>
					<td> {{ $ventas['totales']['vend']['boletos'] }} bol. / {{ number_format($ventas['totales']['vend']['euros'],2,',','.') }} €</td>
				</tr>
			</table>    			
    	</div>
    </div>
</div>
</div>
@endsection


