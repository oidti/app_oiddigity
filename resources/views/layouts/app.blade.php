<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta http-equiv="Expires" content="0">

    <meta http-equiv="Last-Modified" content="0">

    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">

    <meta http-equiv="Pragma" content="no-cache">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'App OID') }}</title>
    <!-- Styles -->    
    {!! Html::style('assets/css/bootstrap.css') !!}
    
    <!-- Datepicker -->
    {!! Html::style('assets/css/bootstrap-datepicker3.css') !!}
    {!! Html::style('assets/css/bootstrap-datepicker.standalone.css') !!}
    {!! Html::style('assets/css/styles.css') !!}
    {!! Html::style('assets/css/style_inf_recaudacion.css') !!}

</head>
<body>
    <div id="app">
        @auth
            @include('menu2')
        @endauth

        @yield('content')
    </div>
    <hr>
    <footer class="footer">
        <div class="container pie_pagina">
            <center>
            <span class="text-muted ">Organización Impulsora de Discapacitados  © 2018</span>
            <center>
        </div>
    </footer>

</body>
<!-- Scripts -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    {!! Html::script('assets/js/bootstrap.min.js') !!}
    {!! Html::script('assets/js/bootstrap-datepicker.js') !!}
    <!-- Datepicker -->
    {!! Html::script('assets/js/bootstrap-datepicker.es.min.js') !!}
    <!-- Script eventos -->
    {!! Html::script('assets/js/event.js') !!}
    <!-- Menu -->
    {!! Html::script('assets/js/menu.js') !!}
    <!-- <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script> -->

<script>

    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        language: "es",
        autoclose: true
    });
</script>
</html>
