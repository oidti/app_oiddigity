<div id='cssmenu'>
  <ul>
    <li><a href="{{ url('/') }}">Inicio</a></li> 
    <li class='active'><a href='#'>Informes ventas</a>
      <ul>
        <li><a href='{{ route('ventas.delegacion') }}'><span class="glyphicon glyphicon-search" aria-hidden="true"></span>Ventas delegaciones </a>
              
        </li>
        <li><a href='{{ route('ventas.delegacion.desglosado') }}'>Ventas delegaciones desglosado</a>
            
        </li>
      </ul>
    </li>
    <li class='active'><a href='{{ route('recaudacion.delegacion') }}'>Recaudación delegación</a>
     
    </li>
    <li class="navbar-right">
      <a href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Salir
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                         {{ csrf_field() }}
      </form>
    </li> 
  </ul>
</div> 
