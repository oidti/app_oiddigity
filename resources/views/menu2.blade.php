

<nav class="navbar navbar-inverse" role="navigation">
  <!-- El logotipo y el icono que despliega el menú se agrupan
       para mostrarlos mejor en los dispositivos móviles -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand logo" href="{{ url('/') }}"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Inicio</a>
  </div>
 
  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
       otro elemento que se pueda ocultar al minimizar la barra -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <!-- Menu ventas -->
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
         <span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span> Informes ventas<b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="{{ route('ventas.delegacion') }}">Ventas delegacion</a></li>
          <li><a href="{{ route('ventas.delegacion.desglosado') }}">Ventas delegacion desglosado</a></li>
          
        </ul>
      </li>
      <li><a href="{{ route('recaudacion.delegacion') }}"> <span class="glyphicon glyphicon-file" aria-hidden="true"></span> Informe recaudacion delegación</a></li>
     
      <li><a href="{{ route('botes') }}"><span class="glyphicon glyphicon-file" aria-hidden="true"></span>Listado botes</a></li>
     
    </ul>

    <ul class="nav navbar-nav navbar-right">

      <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Salir</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                         {{ csrf_field() }}
        </form>
      </li>      
    </ul>
  </div>
</nav>

