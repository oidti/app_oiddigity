<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');

/** Informes ventas **/

Route::get('/ventas/delegacion', 'Ventas\InformeDelegacionController@index')->name('ventas.delegacion'); //mostrar formulario filtro busqueda
Route::get('/ventas/desglosado/delegacion', 'Ventas\InformeDelegacionDesglosadoController@index')->name('ventas.delegacion.desglosado'); //mostrar formulario filtro busqueda

Route::post('/ventas/delegacion/informe', 'Ventas\InformeDelegacionController@showInforme')->name('ventas.delegacion.informe'); //muestra el informe
Route::post('/ventas/delegacion/desglosado/informe', 'Ventas\InformeDelegacionDesglosadoController@showInforme')->name('ventas.delegacion.desglosado.informe'); //muestra el informe

Route::get('/hidden', ['before' => 'auth', function(){
	$contents = View::make('hidden');
	$response = Response::make($contents, 200);
	$response->header('Expires', 'Tue, 1 Jan 1980 00:00:00 GMT');
	$response->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
	$response->header('Pragma', 'no-cache');
	return $response;
}]);

/** Informes recaudacion **/
Route::get('/recaudacion/delegacion', 'Recaudacion\InformeRecaudacionDelegacionController@index')->name('recaudacion.delegacion'); //mostrar formulario filtro busqueda
Route::post('/recaudacion/delegacion/informe', 'Recaudacion\InformeRecaudacionDelegacionController@showInforme')->name('recaudacion.delegacion.informe'); //muestra el informe

/** Informes bote **/
Route::get('/botes', 'Bote\ListadoBotesController@index')->name('botes'); //mostrar formulario filtro busqueda
Route::post('/botes/informe', 'Bote\ListadoBotesController@showInforme')->name('botes.informe'); //muestra el informe